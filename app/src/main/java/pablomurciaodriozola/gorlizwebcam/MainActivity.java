package pablomurciaodriozola.gorlizwebcam;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import uk.co.senab.photoview.PhotoViewAttacher;

public class MainActivity extends AppCompatActivity {

    //TODO: IMAGENES
    //TODO: ANUNCIOS
    //TODO: No hay servicio
    //TODO: Styles under API 21
    //TODO: Share button
    //TODO: Fullscreen go back & force landscape
    //TODO: Remind rate app (appirater)

    //VIEW OBJECTS
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView leftDrawerList;
    private ImageView mainImageView;
    private PhotoViewAttacher photoViewAttacher;
    private FloatingActionButton floatingButton;

    //HELPER OBJECTS
    private ArrayAdapter<String> navigationDrawerAdapter;
    private RequestQueue volleyQueue;
    private Handler handler = new Handler();
    private Runnable runnable;

    //DATA OBJECTS
    private String[] codesArray;
    private String[] namesArray;

    private int refreshDelay = 500;
    private boolean isLoading = false;
    private boolean isFullScreen = false;
    public static final String VOLLEY_TAG = "MyTag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initData();
        initView();
        initDrawer();
        setListeners();
    }

    @Override
    protected void onStart() {
        if (isLandscape(getApplicationContext())){
            floatingButton.show();
        } else {
            floatingButton.hide();
        }

        isLoading = false;

        handler.postDelayed(new Runnable() {
            public void run() {
                if (!isLoading){
                    loadImage();
                }
                runnable=this;
                handler.postDelayed(runnable, refreshDelay);
            }
        }, refreshDelay);

        super.onStart();
    }

    private void initData(){
        namesArray = getResources().getStringArray(R.array.names_string_array);
        codesArray = getResources().getStringArray(R.array.codes_string_array);
    }

    private void initView() {
        mainImageView = (ImageView) findViewById(R.id.mainImageView);
        leftDrawerList = (ListView) findViewById(R.id.left_drawer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        floatingButton = (FloatingActionButton) findViewById(R.id.floating_button);

        navigationDrawerAdapter=new ArrayAdapter<>( MainActivity.this, android.R.layout.simple_list_item_1, namesArray);
        leftDrawerList.setAdapter(navigationDrawerAdapter);

        if (toolbar!=null){
            toolbar.setTitle(R.string.app_name);
            setSupportActionBar(toolbar);
        }

        photoViewAttacher = new PhotoViewAttacher(mainImageView);
    }

    private void initDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawerLayout.setDrawerListener(drawerToggle);

        leftDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                changeCameraFocus(i);
                drawerLayout.closeDrawers();
            }
        });
    }

    private void setListeners() {
        floatingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleFullScreen();
            }
        });
    }

    private void changeCameraFocus(int index){
        if (volleyQueue == null){
            volleyQueue = Volley.newRequestQueue(this);
        }

        String url = getString(R.string.focusURLPrefix) + codesArray[index];

        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

            }
        },new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        volleyQueue.add(stringRequest);
    }

    private void loadImage(){
        isLoading = true;
        Picasso.with(this)
                .load(getString(R.string.imageUrl))
                .noPlaceholder()
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .into(mainImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        isLoading = false;
                        //Prevent update if user is zooming
                        if (photoViewAttacher.getScale()==1){
                            photoViewAttacher.update();
                        }
                    }

                    @Override
                    public void onError() {
                        isLoading = false;
                    }
                });
    }

    private void toggleFullScreen() {
        if(isFullScreen) {
            isFullScreen=false;
            getSupportActionBar().show();
            floatingButton.show();
            photoViewAttacher.setScaleType(ImageView.ScaleType.FIT_CENTER);

        }else{
            isFullScreen=true;
            getSupportActionBar().hide();
            floatingButton.hide();
            photoViewAttacher.setScaleType(ImageView.ScaleType.FIT_XY);
        }

        //SET INMERSIVE MODE
        int newUiOptions = getWindow().getDecorView().getSystemUiVisibility();

        if (Build.VERSION.SDK_INT >= 14) {
            newUiOptions ^= mainImageView.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        }

        if (Build.VERSION.SDK_INT >= 16) {
            newUiOptions ^= mainImageView.SYSTEM_UI_FLAG_FULLSCREEN;
        }

        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= mainImageView.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }

        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);
    }

    @Override
    public void onBackPressed() {
        if (isFullScreen && isLandscape(getApplicationContext())){
            toggleFullScreen();
        } else {
            super.onBackPressed();
        }
    }

    private static boolean isLandscape(Context context){
        int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();

        if (rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270){
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        // Handle action buttons (Los del toolbar no los del drawer)
        switch(item.getItemId()) {
            //           case R.id.action_settings:
            //                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);
        isLoading = false;
        super.onPause();
    }

    @Override
    protected void onStop () {
        super.onStop();
        if (volleyQueue != null) {
            volleyQueue.cancelAll(VOLLEY_TAG);
        }
    }
}